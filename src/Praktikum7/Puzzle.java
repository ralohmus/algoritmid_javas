package Praktikum7;


import java.util.ArrayList;

/**
 * String puzzle class, takes 3 inputs from command line and finds how many solutions are there,
 * when first word + second word equals third word and every character have decimal value.
 *
 * @Author Rauno Lõhmus
 */
public class Puzzle {
    private void setCharValue(long charValue) {
        this.charValue = charValue;
    }

    private long charValue = 0;
    private char character;

    /**
     * Get method for charValue
     *
     * @return charValue
     */
    private long getCharValue() {
        return charValue;
    }

    /**
     * Method to add 1 to charValue.
     *
     * @param charValue current charValue
     */
    private void increment(long charValue) {
        this.charValue = charValue + 1;
    }

    /**
     * Get method for character
     *
     * @return returns current character.
     */
    private char getCharacter() {
        return character;
    }

    /**
     * Puzzle object constructor.
     *
     * @param charValue value of character.
     * @param character character.
     */
    private Puzzle(long charValue, char character) {
        this.charValue = charValue;
        this.character = character;
    }

    /**
     * Solve the word puzzle.
     *
     * @param args three words (addend1 addend2 sum)
     */
    public static void main(String[] args) {
        solvePuzzle(args[0], args[1], args[2]);
    }

    /**
     * makes arrayList of puzzle elements
     *
     * @param first  first word
     * @param second second word
     * @param sum    equals word
     * @return return arrayList of puzzle element
     */
    private static ArrayList<Puzzle> makeArrayList(String first, String second, String sum) {
        char[] wordsArrayList = (first + second + sum).toCharArray();
        ArrayList<Puzzle> arrayList = new ArrayList<>();
        for (int i = 0; i < wordsArrayList.length; i++) {
            if (find(wordsArrayList, wordsArrayList[i], i) == Integer.MIN_VALUE) {
                arrayList.add(new Puzzle(0, wordsArrayList[i]));
            } else if (arrayList.size() >= 11)
                throw new RuntimeException("In three words there is too many different characters, 10 is maximum");
        }
        return arrayList;
    }

    /**
     * Method, which finds if character is already used.
     *
     * @param a char array where is all words characters
     * @param b the character what we are looking for
     * @param c index, how long we will go with our array
     * @return returns 0 if, this character is not used before, otherwise returns this character
     * index in char array.
     */
    private static int find(char[] a, char b, int c) {
        int y = Integer.MIN_VALUE;
        for (int i = 0; i < c; i++) {
            if (a[i] == b) {
                y = i;
            }
        }
        return y;
    }

    /**
     * Method which calculates two words, and checks if 2 word sum in same as third word.
     *
     * @param a          ArrayList of puzzle elements.
     * @param first      First word.
     * @param second     Second word.
     * @param sum        Third word.
     * @param firstChar  First word, first character.
     * @param secondChar Second word, first character.
     * @param sumChar    Third word, third character.
     * @return Returns boolean if first word + second word equals third word.
     */
    private static boolean tootle(ArrayList<Puzzle> a, String first, String second, String sum, char firstChar, char secondChar, char sumChar) {
        long firstWordValue = 0;
        long secondWordValue = 0;
        long thirdWordValue = 0;
        char c[] = (first + second + sum).toCharArray();
        long[] b = new long[(sum + second + first).length()];
        for (int j = 0; j < (sum + second + first).length(); j++) {
            for (Puzzle anA : a) {
                if (anA.getCharacter() == firstChar || anA.getCharacter() == secondChar || anA.getCharacter() == sumChar) {
                    if (anA.getCharValue() == 0) {
                        return false;
                    }
                }
                if (anA.getCharacter() == c[j]) {
                    b[j] = anA.getCharValue();
                    if (j < first.length()) {
                        firstWordValue += b[j] * Math.pow(10, (first.length() - j - 1));
                    } else if (j >= first.length() && j < (second.length() + first.length())) {
                        secondWordValue += b[j] * Math.pow(10, (second.length() - j + (first.length() - 1)));
                    } else {
                        thirdWordValue += b[j] * Math.pow(10, (sum.length() - j + (second.length() + first.length() - 1)));
                    }
                    break;
                }
            }
        }
        return firstWordValue + secondWordValue == thirdWordValue;
    }

    /**
     * Method to check array if every character value is different.
     *
     * @param a ArrayList of puzzle elements.
     * @return true if every character have different value, and false if not.
     */
    private static boolean checkArray(ArrayList<Puzzle> a) {
        long[] mLong = new long[10];
        for (Puzzle anA : a) {
            int index = (int) anA.getCharValue();
            mLong[index]++;
            if (mLong[index] > 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to solve the string puzzle.
     *
     * @param first  first word.
     * @param second second word.
     * @param sum    third word.
     */
    private static void solvePuzzle(String first, String second, String sum) {
       if(first.matches("[a-yA-Y]"))
           throw new RuntimeException("String contains non characters. Wrong word is " + first);
        if(second.matches("[a-yA-Y]"))
            throw new RuntimeException("String contains non characters. Wrong word is " + second);
        if(sum.matches("[a-yA-Y]"))
            throw new RuntimeException("String contains non characters. Wrong word is " + sum);
        if (first.length() > 18)
            throw new RuntimeException("String is too long: " + first + " and its length is " + first.length());
        if (second.length() > 18)
            throw new RuntimeException("String is too long: " + second + " and its length is " + second.length());
        if (sum.length() > 18)
            throw new RuntimeException("String is too long: " + sum + " and its length is " + sum.length());
        System.out.printf("Finding solutions for words %S %S %S \n", first, second, sum);
        char firstChar[] = first.toCharArray();
        char secondChar[] = second.toCharArray();
        char sumChar[] = sum.toCharArray();
        int printedSolutions = 0;
        ArrayList<Puzzle> puzzleArrayList = makeArrayList(first, second, sum);
        int numberOfSolutions = 0;
        int n = puzzleArrayList.size() - 1;
        do {
            if (puzzleArrayList.get(n).getCharValue() <= 9) {
                if (checkArray(puzzleArrayList)) {
                    if (tootle(puzzleArrayList, first, second, sum, firstChar[0], secondChar[0], sumChar[0])) {
                        numberOfSolutions++;
                        printedSolutions++;
                        if (printedSolutions == 1) {
                            System.out.println("This string puzzle solutions are");
                        }
                        printSolutions(puzzleArrayList);
                    }
                }
            } else {
                while (puzzleArrayList.get(n).getCharValue() >= 9) {
                    puzzleArrayList.get(n).setCharValue(0);
                    n--;
                    if (n < 0) break;
                }
                if (n >= 0) {
                    puzzleArrayList.get(n).increment(puzzleArrayList.get(n).getCharValue());
                    n = puzzleArrayList.size() - 1;
                    puzzleArrayList.get(n).setCharValue(-1);
                }
            }
            if (n > 0) {
                puzzleArrayList.get(n).increment(puzzleArrayList.get(n).getCharValue());
            }
        } while (n >= 0);
        if (numberOfSolutions == 0) {
            System.out.println("Not a single solutions was found");
        } else if (numberOfSolutions > 1) {
            System.out.println("With these words we found " + numberOfSolutions + " solutions!");
        } else {
            System.out.println("With these words we found " + numberOfSolutions + " solution!");
        }
    }

    /**
     * This method prints out a solution
     *
     * @param puzzleArrayList takes in arrayList of puzzle elements
     */
    private static void printSolutions(ArrayList<Puzzle> puzzleArrayList) {
        for (Puzzle aPuzzleArrayList : puzzleArrayList) {
            System.out.printf("%S=%d ", aPuzzleArrayList.getCharacter(),
                    aPuzzleArrayList.getCharValue());
        }
        System.out.println();
    }
}


