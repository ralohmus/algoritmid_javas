package Praktikum1;

public class ColorSort {

    enum Color {red, green, blue}


    public static void main(String[] param) {
        Color[] m = new Color[5];
        m[0] = Color.blue;
        m[1] = Color.green;
        m[2] = Color.red;
        m[3] = Color.red;
        m[4] = Color.blue;
        int a = 9;
        int b = 6;
        System.out.println(9<b -1);

        reorder(m);

        for (Color itme : m
                ) {
            System.out.println(itme);

        }


    }

    /**
     * See meetod sorteerib Color tüüpi massiivi, järjestades punane, roheline, sinise.
     *
     * @param balls Color tüüpi masiiv
     */
    public static void reorder(Color[] balls) {
        int punaneLoendus = 0;
        int rohelineLoendus = 0;
        for (int i = 0; i < balls.length; i++) {
            switch (balls[i]) {
                case red:
                    punaneLoendus++;
                    break;
                case green:
                    rohelineLoendus++;
                    break;
            }
        }

        for (int i = 0; i < balls.length; i++) {
            if (punaneLoendus >= 1) {
                balls[i] = Color.red;
                punaneLoendus--;
            } else if (rohelineLoendus >= 1) {
                balls[i] = Color.green;
                rohelineLoendus--;
            } else  {
                balls[i] = Color.blue;
            }
        }


    }


}