package praktikum6;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Subject Algorithms and data structures homework No. 6 exercise
 * Used materials:
 * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 * https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html
 */
public class GraphTask {

    /**
     * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    private void run() {
        System.out.println("Siseta arv, mitu punkti sa graafi soovid");
        int a = TextIO.getlnInt();
        System.out.println("Sistea arv mitu kaart sul punktide vahel on");
        int b = TextIO.getlnInt();
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(a, b);
        System.out.println(g);
    }

    /**
     * Calculates weight number, which will be random number.
     *
     * @return return weight number.
     */
    private int calcWeight() {
        return  ThreadLocalRandom.current().nextInt(1, 9 );
    }

    /**
     * Object for vertex
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;//Arc weight

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
            info = 0;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Object graph, what contains vertexes and 2 dimensional arcs.
     */
    class Graph {
        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(40);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    sb.append(" and arc length is ").append(a.info);
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Method for creating an Vertex to graph.
         *
         * @param vid Vertex name.
         * @return Vertex object
         */
        private Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Method for creating Arc to graph.
         *
         * @param aid  Arc name
         * @param from Vertex where we start
         * @param to   Vertex where we finish
         * @return Arc object between from vertex and to vertex.
         */
        private Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        private void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int weight = calcWeight();
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]).info = weight;
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]).info = weight;
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        private int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j] = a.info;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        private void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            if (n <= 3) {
                int edgeCount = m - n + 1;  // remaining edges
                while (edgeCount > 0) {
                    int i = (int) (Math.random() * n);  // random source
                    int j = (int) (Math.random() * n);  // random target
                    if (i == j)
                        continue;  // no loops
                    if (connected[i][j] != 0 || connected[j][i] != 0)
                        continue;  // no multiple edges
                    Vertex vi = vert[i];
                    Vertex vj = vert[j];
                    int weight = calcWeight();
                    connected[i][j] =
                            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj).info = weight;
                    connected[j][i] = createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi).info = weight;
                    edgeCount--;  // a new edge happily created
                }
            }
            //this calculates the longest path in graph.
            distMatrix(connected);
            shortestPathTable(connected);
            shortestPathTable(connected);
            int[] longestPath = findLongestPath(connected);
            System.out.println("The longest path in Graph is between v" + (longestPath[1] + 1) + " and v" +
                    (longestPath[2] + 1) + ". Distance is: " + longestPath[0]);
            for (int[] aConnected : connected) {
                for (int anAConnected : aConnected) {
                    System.out.print(anAConnected + " ");
                }
                System.out.println();
            }
        }

        /**
         * Method to make matrix of vertex distance. if the vertex is same in the column and row then the value is 0,
         * other 0 are made to 555555555, it is be bigger than any other given arc weight.
         *
         * @param m takes in matrix
         */
        private void distMatrix(int[][] m) {
            for (int i = 0; i < m.length; i++) {
                for (int j = 0; j < m[i].length; j++) {
                    if (m[i][j] == 0 && i != j) {
                        m[i][j] = 555555555;
                    }
                }
            }
        }

        /**
         * This method calculates path for all given vertexes, even for the nodes, which are not connected.
         * If column vertex and row vertex are the same, then distance value is be 0.
         * The method is based on Floyd-Warshall algorithm
         *
         * @param m takes in matrix of vertex distance.
         */
        private void shortestPathTable(int[][] m) {
            int length = 0;
            for (int k = 0; k < m.length; k++) {
                for (int i = 0; i < m.length; i++) {
                    for (int j = 0; j < m.length; j++) {
                        if (m[i][j] > (m[i][k] + m[k][j]) && i != j) {
                            m[i][j] = (m[i][k] + m[k][j]);
                        } else if (m[i][j] > length) {
                            length = m[i][j];
                        }
                    }
                }

            }
        }
    }

    /**
     * This method finds then longest path in graph.
     *
     * @param m This method requires matrix of vertex distance
     * @return it returns array which first element is path distance and 2 and 3 element gives 2 vertexes, which
     * distance is the longest, but it returns index of matrix so you have to add 1 to it. Example: (v(index) +1).
     */
    private int[] findLongestPath(int[][] m) {
        int longestPath = 0;
        int column = 0;
        int row = 0;
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (m[i][j] > longestPath) {
                    longestPath = m[i][j];
                    column = j;
                    row = i;
                }
            }
        }
        return new int[]{longestPath, column, row};
    }
}
