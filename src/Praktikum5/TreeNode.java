package Praktikum5;



import java.util.Stack;

public class TreeNode {
    // muutuja, mis on elemendi nimi
    private String name;
    // muutuja, mis on elemndist all pool olev element
    private TreeNode firstChild;
    // muutja, mis on kõrval olev element
    private TreeNode nextSibling;
    // konstruktor
    TreeNode(String n, TreeNode d, TreeNode r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    /**
     * Viide: http://interactivepython.org/runestone/static/pythonds/Trees/ParseTree.html, sõbra abi
     * Meetod mis teeb, sõne kujul puu node tüübi puuks
     * @param s apuu sõne kujul
     * @return tagastab node, mis on puu kujuline
     */
    public static TreeNode parsePrefix(String s) {
        if (s.contains("((")) throw new RuntimeException("Avaldis on vale, leidub Kahekordsed sulud. Vigane avaldis on: " + s);
        else if (s.contains(",,")) throw new RuntimeException("Avaldis on vale, leidub kahekordsed komad. Vigane avaldis on: " + s);
        else if (s.contains("()")) throw new RuntimeException("Avaldis on vale, leidub tühi haru. Vigane avaldis on: " + s);
        else if (s.trim().length() == 0) throw new RuntimeException("Avaldis on tühi. Vigane avaldis on: " + s);
            //else if (s.replace(" ", "").length() != s.length()) throw new RuntimeException("Avaldis on vale, Juure nimes on tühik" + s);
        else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) throw new RuntimeException("Avaldis on vale. Juures ei saa olla mitu elementi " + s);
        else if (s.contains(",(")) throw new RuntimeException("Avaldis on vale. Enne sulgu ei tohi olla koma " + s);
        else if (s.contains(")(")) throw new RuntimeException("Avaldis on vale. Lõppeva ja algava sulu vahel peab node olema " + s);
            //else if (!s.matches("[\\w(),+*/-]+")) throw new RuntimeException("Avaldis on vale. Avaldises on lubamatuid märke " + s);
        else if (s.startsWith("(") || s.startsWith(")") || s.startsWith(",")) throw new RuntimeException("Avaldis on vale. Avaldis peab algama nodega, mitte märgiga");
        byte algus = 0;
        byte lopp = 0;
        Stack<TreeNode> treeNodeStack = new Stack<>();
        String[] m = s.split("");
        if (m[m.length - 1].trim().equals(",") || m[m.length-1].trim().equals("(")) throw new RuntimeException("Avaldis on vale. Vigane avaldis on: " + s + " Viga on: " + m[m.length -1]);
        TreeNode treeNode = new TreeNode(null, null, null);
        boolean root = false;
        for (int i = 0; i < m.length; i++) {

            /** if (m[i].equals("("))
             algus++;
             if (m[i].equals(")"))
             lopp++; **/
            switch (m[i]) {
                case "(":
                    algus++;
                    if (root) throw new RuntimeException("Juures ei sa olla mitu elementi" + s);
                    treeNodeStack.push(treeNode);
                    treeNode.firstChild = new TreeNode(null, null, null);
                    treeNode = treeNode.firstChild;
                    if (m[i + 1].trim().equals(",")) throw new RuntimeException("Element ei saa olla koma" + s);
                    break;
                case ",":
                    if (root) throw new RuntimeException("Mitu elemnti ei tohi olla juures" + s);
                    treeNode.nextSibling = new TreeNode(null, null, null);
                    treeNode = treeNode.nextSibling;
                    break;
                case ")":
                    lopp++;
                    if (treeNodeStack.isEmpty()) throw new RuntimeException("Avaldis on vale. " + s + " viga on: " + m[i]);
                    treeNode = treeNodeStack.pop();
                    if (treeNodeStack.size() == 0) {
                        root = true;
                    }
                    break;
                default:
                    if (treeNode.name == null) {
                        treeNode.name = m[i].trim();
                    } else {
                        if (m[i -1 ].trim().equals(")")) throw new RuntimeException("Avaldis on vale. Siblingud peavad olema eraltatud komaga " + s);
                        treeNode.name += m[i].trim();

                    }
                    break;
            }
        }
        if (lopp != algus) throw new RuntimeException("Avaldis on vale. Algavid sulge ja lõppevaid sulge pole sama palju" + s);
        return treeNode;
    }

    /**
     * Meetod, mis teeb node kujul puu sõneks, aga mis algab paremalt poolt
     * @return puu, mis on sõne kujul
     */
    public String rightParentheticRepresentation() {
        StringBuilder b = new StringBuilder();
        if (name == null) throw new RuntimeException("Antud puud ei eksisteeri!");
        if (name.trim().length() == 0) throw new RuntimeException("Puu juur on tühi!");
        if (firstChild != null) {
            b.append("(");
            b.append(firstChild.rightParentheticRepresentation());
            b.append(")");
        }
        if (nextSibling != null) {
            b.append(name);
            b.append(",");
            b.append(nextSibling.rightParentheticRepresentation());
        } else {
            b.append(name);
        }
        return b.toString();
    }
    // Main meetod
    public static void main(String[] param) {
        String s = "ABC(3,(gg),4)";
        TreeNode t = TreeNode.parsePrefix(s);
        String v = t.rightParentheticRepresentation();
        System.out.println(s + " ==> " + v ); // A(B1,C,D) ==> (B1,C,D)A


    }
}