package Praktikum4;

import java.util.Arrays;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    private long numerator, denominator;

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param)
    {

     System.out.println( valueOf("2/-4"));
    }

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b != 0) {
            this.numerator = a;
            this.denominator = b;

            //see kood taadnab murru
            //http://www.cs.ioc.ee/DM2/Handouts/10_Arvuteooria1.pdf
            long factor = 0;
            long gcd = 0;
            while (b != 0) {
                factor = b;
                b = a % b;
                a = factor;
            }
            gcd = a;
            this.numerator = numerator / gcd;
            this.denominator = denominator / gcd;
            if (denominator < 0) {
                this.numerator = this.numerator * -1;
                this.denominator = this.denominator * -1;
            }
        } else throw new RuntimeException("Objekti pole võimalik luua. Nimetaja on: " + b);
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return String.format("%d%s%d", numerator, "/", denominator);

    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        return numerator == ((Lfraction) m).numerator && denominator == ((Lfraction) m).denominator;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     * @authot Shimon Schocken
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (int) (prime * result + denominator);
        result = (int) (prime * result + numerator);
        return result;
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        if (this.denominator == m.denominator) {
            return new Lfraction(this.numerator + m.numerator, this.denominator);
        }
        return new Lfraction((m.numerator * this.denominator) + this.numerator * m.denominator, m.denominator * this.denominator);
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        return new Lfraction(m.numerator * this.numerator, m.denominator * this.denominator);
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (this.numerator != 0)
            return new Lfraction(this.denominator, this.numerator);
        else
            throw new ArithmeticException("Murru nimetajat ja luegjat ei ole võimalik vahetada, sest lugeja on: " + numerator);

    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction((-1 * this.numerator), this.denominator);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        if (this.denominator == m.denominator) {
            return new Lfraction((this.numerator - m.numerator), this.denominator);
        }
        return new Lfraction((this.numerator * m.denominator) - (m.numerator * this.denominator), m.denominator * this.denominator);
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (m.numerator != 0)
            return new Lfraction(this.numerator * m.denominator, this.denominator * m.numerator);
        else
            throw new ArithmeticException("Jagamist ei ole võimalik teha, sest lugeja on 0 ning siis toimuks 0 jagamine");
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
       if (this == m)
           return 0;
       else if (this.numerator * this.denominator == m.numerator * m.denominator)
           return 0;
       else if (this.numerator * this.denominator < m.numerator * m.denominator)
           return -1;
       else
           return 1;
}

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            Lfraction lfraction = new Lfraction(this.numerator, this.denominator);
            return lfraction;
        } catch (RuntimeException e) {
            throw new RuntimeException("Kloonimine ei olnud võimalik");
        }
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return numerator / denominator;
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        if (integerPart() == 0)
            return this;
        else if (integerPart() != 0) {
            numerator = numerator - (integerPart() * denominator);
        }
        return this;
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) numerator / denominator;
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        if (d > 0)
            return new Lfraction(Math.round(f * d), d);
        else
            throw new RuntimeException("Murru nimetaja ei saa olla 0 või negatiivne" + d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (!s.contains("/")) throw new RuntimeException("Error, Avaldis on vale kujul, puudub / " + s);
        if (s.trim().length() == 0) throw new RuntimeException("Avaldis on vale!" + s);
        char[] sChar = s.toCharArray();
        byte operaator = 0;
        for (char c : sChar) {
            if (c == '/')
                operaator++;
        }
        if (operaator > 1) throw new RuntimeException("Avaldis on vale, liiga palju '/' " + s);
        long a = 0;
        long b = 0;
        String[] m = Arrays.stream(s.split("/")).map(String::trim).toArray(String[]::new);
        if (m.length <=1) throw new RuntimeException("Avaldis on vale. Avaldis on:  " + s);
        if (m.length >= 3) {
            throw new RuntimeException("Avaldis on vale. Seal on üleliigseid asju: " + s);
        }

        try {
            a = Long.parseLong(m[0]);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Avaldis on vale. Vigane avaldis on: " + s +" Viga on lugejas: "+ m[0]);
        }
        try {

            b = Long.parseLong(m[1]);

        } catch (RuntimeException e) {
            throw new NumberFormatException("Avadis on vale. Vigane avaldis on: " + s + " Viga on nimetajas: " +m[1]);
        }
        if (Long.parseLong(m[1]) != 0) {
            return new Lfraction(a, b);
        } else throw new RuntimeException("Sõne ei saa muuta murruks, kuna nimetja on: 0" + s);
    }
}