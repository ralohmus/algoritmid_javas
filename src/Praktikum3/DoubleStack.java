package Praktikum3;

import java.util.*;

// kasutatud abina http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
public class DoubleStack {


    public static void main(String[] args) {

        System.out.print(interpret("5 +"));

//        DoubleStack stack = new DoubleStack();
//        stack.pop();

    }

    // stacki linkedlist
    private LinkedList<Double> list = new LinkedList<>();

    //Konstruktor Stacki jaoks
    DoubleStack() {
    }

    /**
     * see meetod üritab kloonida stacki
     *
     * @return tagastab koopa stacki
     * @throws CloneNotSupportedException kui kloonimine ebaõnnestus
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            DoubleStack ajutine = new DoubleStack();
            ajutine.list.addAll(list);
            return ajutine;
        } catch (Exception e) {
            throw new CloneNotSupportedException("kloonimine ei olnud võimalik");
        }
    }

    /**
     * meetod mis näitab kas stack on tühi või mitte
     *
     * @return tagastab kas stack on tühi või mitte (true, false)
     */
    public boolean stEmpty() {
        return list.isEmpty();
    }

    /**
     * Lisab stacki elemendi
     *
     * @param a element mis lisatakse stacki
     */
    public void push(double a) {
        list.push(a);
    }

    /**
     * Võtab stackist ühe elemndi ära
     *
     * @return stacki pealmine element
     */
    public double pop() {
        if (stEmpty())
            throw new RuntimeException("ei saa stackist elementi eemaldada, kuna stack on tühi! Vigane stack on: " + list);
        return list.pop();
    }


    /**
     * meetod mis arutab stacki sees olevate arvudega
     *
     * @param s annab ette mis operatsiooni peaks kasutama
     */
    public void op(String s) {
        if (!stEmpty()) {
            double a = list.pop();
            double b = list.pop();
            switch (s) {
                case "+":
                    list.push(a + b);
                    break;
                case "-":
                    list.push(b - a);
                    break;
                case "*":
                    list.push(a * b);
                    break;
                case "/":
                    list.push(b / a);
                    break;
            }
        } else {
            throw new RuntimeException("Ei ole võimalik arvutada, kuna stack on tühi. Vigane stack on: " + list);
        }
    }

    /**
     * Saab piilida, mis on stacki sees
     *
     * @return returnib väärtuse
     */
    public double tos() {
        if (stEmpty())
            throw new RuntimeException("Ei ole võimalik piiluda stacki sisse, kuna elemendid stackis puuduvad. Vigane stack on: " + list);
        return list.getFirst();
    }

    /**
     * Võrdleb kahte stacki
     *
     * @param o võtab sisse objekti
     * @return tagastab väärtuse kas on võrdsed või mitte
     */
    @Override
    public boolean equals(Object o) {
        return this.list.equals(((DoubleStack) o).list);
    }

    /**
     * Teeb stacki sõne kujuks
     *
     * @return tagastab stringi
     */
    @Override
    public String toString() {
        if (stEmpty()) return "tyhi";
        StringBuilder stringBuffer = new StringBuilder();
        for (int i = list.size() - 1; i >= 0; i--) {
            stringBuffer.append(String.valueOf(list.get(i) + " "));
        }
        return stringBuffer.toString();
    }

    /**
     * kontrollib, kas sõne kujul esitatud arv on ikka arv
     *
     * @param s sõne
     * @return tagastab kas on arv, mis on sõne kujul
     */
    public static boolean isNumberic(String s) {
        try {
            Double.parseDouble(s);
        } catch (RuntimeException ex) {
            return false;
        }
        return true;
    }

    /**
     * https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
     *
     * @param pol sõne kujul avaldis
     * @return tagastab interpeeritud väärtuse
     * @throws RuntimeException tagsatab erroro, kui avaldis pole korrektne
     */
    public static double interpret(String pol) throws RuntimeException {
        long number = 0;
        if (pol.trim().length() == 0) throw new RuntimeException("Avaldis on vale. Avaldis on tühi" + pol);
        else if (pol.equals(null)) throw new NullPointerException("Avaldis on vale. Avaldis on null" + pol);
        else if (pol.equals("")) throw new RuntimeException("Avaldis on vale. String on tühi" + pol);
        DoubleStack stack = new DoubleStack();
        ArrayList<String> ar = new ArrayList<>();
        StringTokenizer m = new StringTokenizer(pol);
        String operaator = "+-*/";
        while (m.hasMoreElements()) {
            ar.add(m.nextToken());
        }
        number = ar.parallelStream().filter(s -> isNumberic(s)).count();
        if (operaator.contains(ar.get(0)))
            throw new RuntimeException("Avaldis on vale! Esimene liige ei tohi olla opratsioon. Avaldises: " + pol + " Ja viga on " + ar.get(0));
        if (number <= ar.size() / 2)
            throw new RuntimeException("Avaldis on vale. Operatsioone on liiga palju avaldises: " + pol + " Operatsioon: " + ar.get(ar.size() - 1) + " on üleliigne");
        if (ar.size() == 1 && isNumberic(ar.get(0)))
            return Double.parseDouble(ar.get(0));
        else if (isNumberic(ar.get(ar.size() - 1)))
            throw new RuntimeException("Avaldis on vale! Viimane element ei tohi olla number! Avaldis: " + pol + " Viga on: " + ar.get(ar.size() - 1));
        for (int i = 0; i < ar.size(); i++) {
            try {
                if (!operaator.contains(ar.get(i))) {
                    stack.push(Double.parseDouble(ar.get(i)));
                } else {
                    stack.op(ar.get(i));
                }
            } catch (RuntimeException ex) {
                throw new RuntimeException("Avaldis on vale! Viga on " + ar.get(i) + " Avaldises: " + pol);
            }
        }
        if (stack.list.size() != 1) throw new RuntimeException("Avaldis on vale! " + pol);
        return stack.pop();
    }
}

